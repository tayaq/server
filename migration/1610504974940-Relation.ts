import {MigrationInterface, QueryRunner} from "typeorm";

export class Relation1610504974940 implements MigrationInterface {
    name = 'Relation1610504974940'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `products` ADD `isActive` tinyint NOT NULL DEFAULT 0");
        await queryRunner.query("ALTER TABLE `products` CHANGE `feature` `feature` varchar(255) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `products` CHANGE `feature` `feature` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `products` DROP COLUMN `isActive`");
    }

}
