import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm'

import { UsersModule } from './users/users.module';
import { CategoriesModule } from './categories/categories.module';
import { ProductsModule } from './products/products.module';
import { PagesModule } from './pages/pages.module';
import { OptionsModule } from './options/options.module';
import { NewsModule } from './news/news.module';
import { ImagesModule } from './images/images.module';
import { OrdersModule } from './orders/orders.module';
require('dotenv').config()

declare var process : {
    env: {
        TYPEORM_HOST: string
        TYPEORM_PORT: number
        TYPEORM_USERNAME: string
        TYPEORM_PASSWORD: string
        TYPEORM_DATABASE: string
        TYPEORM_SYNCHRONIZE: boolean
    }
}

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: process.env.TYPEORM_HOST,
            port: process.env.TYPEORM_PORT,
            username: process.env.TYPEORM_USERNAME,
            password: process.env.TYPEORM_PASSWORD,
            database: process.env.TYPEORM_DATABASE,
            entities: [__dirname + '/**/*.entity{.ts,.js}'],
            synchronize: true,
            autoLoadEntities: true,
            migrationsTableName: "migration",
            migrations: [__dirname + './src/migration/*.{.ts,.js}'],
            extra: {
                charset: "utf8mb4_unicode_ci"
            }
        }),
        UsersModule,
        CategoriesModule,
        ProductsModule,
        PagesModule,
        OptionsModule,
        NewsModule,
        ImagesModule,
        OrdersModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
