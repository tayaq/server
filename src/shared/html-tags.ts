import { Column } from "typeorm";

export abstract class HtmlTags {

    @Column()
    title: string;

    @Column()
    description: string;

}
