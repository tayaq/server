import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

import { Category } from "./category.entity";
import { CategoriesService } from "./categories.service";
import { CreateCategoryDto } from "./dto/create-category.dto"

@Controller('categories')
export class CategoriesController {
    constructor(private readonly categoriesService: CategoriesService) {
    }

    @Get()
    findAll(): Promise<Category[]> {
        return this.categoriesService.findAll();
    }

    @Post()
    createOption(@Body() CreateCategoryDto: CreateCategoryDto): Promise<Category> {
        return this.categoriesService.createCategory(CreateCategoryDto);
    }

    @Put(':id')
    updateOption(@Param('id') id: number, @Body() CreateCategoryDto: CreateCategoryDto) {
        return this.categoriesService.updateCategory(id, CreateCategoryDto);
    }

    @Delete(':id')
    deleteOption(@Param('id') id: number) {
        return this.categoriesService.deleteCategory(id);
    }

}
