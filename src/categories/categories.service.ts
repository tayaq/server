import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";

import { Category } from "./category.entity";
import { Repository } from "typeorm";
import { CreateCategoryDto } from "./dto/create-category.dto"

@Injectable()
export class CategoriesService {
    constructor(
        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>,
    ) {
    }

    findAll(): Promise<Category[]> {
        return this.categoryRepository.find();
    }

    async createCategory(CreateCategoryDto: CreateCategoryDto): Promise<Category> {
        return this.categoryRepository.save(CreateCategoryDto);
    }

    async updateCategory(id: number, CreateCategoryDto: CreateCategoryDto) {
        return this.categoryRepository.update(id, CreateCategoryDto);
    }

    async deleteCategory(id: number) {
        return this.categoryRepository.delete(id);
    }

}
