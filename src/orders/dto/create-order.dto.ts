import { IsEmail, IsEmpty, IsNotEmpty } from 'class-validator';

export class CreateOrderDto {
    list: string
    status: number
    name: string
    phone: string
    street: string
    house: string
    entrance: string
    floor: string
    flat: string
    comment: string
    delivery: boolean
}
