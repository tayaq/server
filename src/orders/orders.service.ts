import { HttpService, Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { Order } from "./order.entity";
import { CreateOrderDto } from "./dto/create-order.dto";

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(Order)
        private readonly ordersRepository: Repository<Order>,
        private http: HttpService
    ) {
    }

    async findAll(): Promise<Order[]> {
        return this.ordersRepository.find({
            order: {
                id: 'DESC'
            },
            take: 30
        });
    }

    async findAllOnStatus(status): Promise<Order[]> {
        return this.ordersRepository.find(
            {
                where: {
                    status
                },
                order: {
                    id: 'DESC'
                }
            }
        );
    }

    async create(CreateOrderDto: CreateOrderDto): Promise<Order> {
        return this.ordersRepository.save(CreateOrderDto);
    }

    async updateStatus(id: number, status: number) {
        return this.ordersRepository.update(id, {
            status
        });
    }

}
