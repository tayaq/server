import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

enum orderStatus {
    CANCELED = 0,
    NEW = 1,
    PROCESS = 2,
    COMPLETED = 3,
}

@Entity('orders')
export class Order {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("json")
    list: string;

    @Column("enum", {enum: orderStatus, default: 1})
    status: orderStatus

    @Column()
    name: string;

    @Column()
    phone: string;

    @Column({nullable: true})
    street: string;

    @Column({nullable: true})
    house: string;

    @Column({nullable: true})
    entrance: string;

    @Column({nullable: true})
    floor: string;

    @Column({nullable: true})
    flat: string;

    @Column({nullable: true})
    comment: string;

    @Column()
    delivery: boolean;

    @CreateDateColumn()
    created: string;

}
