import { Body, Controller, Get, HttpService, Param, Post, Put } from '@nestjs/common';
import { OrdersService } from "./orders.service";
import { CreateOrderDto } from "./dto/create-order.dto";
import { Order } from "./order.entity";
import { Option } from "../options/option.entity";

@Controller('orders')
export class OrdersController {
    constructor(
        private readonly http: HttpService,
        private readonly ordersService: OrdersService
    ) {
    }

    @Get()
    findAll(): Promise<Order[]> {
        return this.ordersService.findAll();
    }

    @Get(':status')
    findAllOnStatus(@Param('status') status: number): Promise<Order[]> {
        return this.ordersService.findAllOnStatus(status);
    }

    @Post('create')
    async createOption(@Body() CreateOrderDto: CreateOrderDto): Promise<Order> {
        try {
            const response = await this.sendOrderToCrm(CreateOrderDto);
        } catch (err) {
            console.log(err);
        }
        return this.ordersService.create(CreateOrderDto);
    }

    @Put('status/:id')
    update(@Param('id') id: number, @Body('status') status: number) {
        return this.ordersService.updateStatus(id, status);
    }

    async getToken() {
        const response = await this.http.get(`https://card.iiko.co.uk:9900/api/0/auth/access_token?user_id=yimyim&user_secret=Yimyim2021`).toPromise();
        return response.data;
    }

    async sendOrderToCrm(dto) {
        const token = await this.getToken();
        const items = [];
        dto.list.forEach(item => {
            items.push({
                id: item.size.crmId,
                amount: item.qty
            })
            if (item.additionals.length > 0) {
                item.additionals.forEach(a => {
                    items.push({
                        id: a.crmId,
                        amount: item.qty
                    })
                })
            }
        });

        return this.http.post(`https://card.iiko.co.uk:9900/api/0/orders/add?&access_token=${token}`, {
                organization: 'b2320000-3838-06a2-e7c5-08d8be10a4f8',
                customer: {
                    name: dto.name,
                    phone: dto.phone,
                },
                order: {
                    items,
                    phone: dto.phone,
                    isSelfService: !dto.delivery,
                    "paymentItems": [
                        {
                            "sum": "0.01",
                            "paymentType": {
                                "id": "09322f46-578a-d210-add7-eec222a08871",
                                "code": "CASH",
                                "name": "Наличные",
                                "comment": null,
                            },
                            "isProcessedExternally": false
                        }
                    ],
                    address: {
                        "street": dto.street,
                        "home": dto.house,
                        "apartment": dto.flat,
                        "entrance": dto.entrance,
                        "floor": dto.floor,
                        "comment": dto.comment
                    },
                }
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }).toPromise();
    }
}
