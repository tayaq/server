import {
    PrimaryGeneratedColumn,
    CreateDateColumn,
    Entity,
    Column,
    ManyToOne,
    JoinColumn,
    ManyToMany,
    JoinTable
} from "typeorm";
import { Product } from "../products/entity/product.entity";

@Entity('images')
export class Image {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    path: string;

    @Column({nullable: true, default: 0})
    productId: number;

    @ManyToOne(type => Product, product => product.images, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    })
    product: Product;

}
