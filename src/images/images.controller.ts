import {
    Body,
    Controller,
    Delete,
    Get,
    HttpStatus,
    Param,
    Post,
    Res,
    UploadedFile,
    UseInterceptors
} from '@nestjs/common';
import { FileInterceptor } from "@nestjs/platform-express";
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ImagesService } from "./images.service";
import { CreateImageDto } from './dto/create-image.dto'
import { CreateProductDto } from "../products/dto/create-product.dto";

@Controller('images')
export class ImagesController {

    constructor(private readonly imagesService: ImagesService) {
    }

    @Get(':folder/:image')
    getImage(@Param('folder') folder, @Param('image') image, @Res() res) {
        const response = res.sendFile(image, {
            root: `./uploads/${folder}/`
        });
        return {
            status: HttpStatus.OK,
            data: response,
        };
    }

    @Post('upload')
    @UseInterceptors(FileInterceptor("image", {
        storage: diskStorage({
            destination: './uploads/temp',
            filename: (req, file, cb) => {
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
                return cb(null, `${randomName}${extname(file.originalname)}`);
            }
        })
    }))
    uploadImage(@UploadedFile() image) {
        return image.filename;
    }

    @Post('save')
    createImage(@Body() CreateImageDto: CreateImageDto) {
        return this.imagesService.saveImages(CreateImageDto);
    }

    @Post('move')
    moveImages(@Body() images) {
        return this.imagesService.moveImages(images);
    }

    @Delete('delete/:id')
    deleteImage(@Param('id') id: number) {
        return this.imagesService.deleteImage(id);
    }

}
