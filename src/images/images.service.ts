import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Option } from "../options/option.entity";
import { Repository } from "typeorm";
import { Image } from "./image.entity";
import { CreateImageDto } from "./dto/create-image.dto";

const fs = require('fs')
import { Product } from "../products/entity/product.entity";

@Injectable()
export class ImagesService {
    constructor(
        @InjectRepository(Image) private readonly imageRepository: Repository<Image>,
    ) {
    }

    async saveImages(CreateImageDto: CreateImageDto): Promise<Image> {
        return this.imageRepository.save(CreateImageDto);
    }

    async getById(id): Promise<Image> {
        return this.imageRepository.findOne({
            where: {id}
        });
    }

    async deleteImage(id: number) {
        let image = await this.getById(id)
        try {
            fs.unlinkSync(`./uploads/products/${image.path}`)
            return this.imageRepository.delete(id);
        } catch (err) {
            return err;
        }

    }

    async moveImages(images) {
        images.forEach(image => {
            let tempPath = `./uploads/temp/${image.path}`
            let newPath = `./uploads/products/${image.path}`
            if (fs.existsSync(tempPath)) fs.rename(tempPath, newPath, (err) => {
                console.log(err);
            });

        });
        // fs.rmdir('./uploads/temp', { recursive: true }, (err) => {
        //     if (err) {
        //         throw err;
        //     }
        //     fs.mkdirSync('./uploads/temp')
        // });

    }

}
