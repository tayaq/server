import {IsEmail, IsEmpty, IsNotEmpty} from 'class-validator';

export class CreateImageDto {
    path: string
    productId: number
}
