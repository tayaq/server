export class CreateOptionProductDto {
    productId: number
    name: string
    price: number
    weight: number
}
