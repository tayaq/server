export class CreateProductDto {
    categoryId: number
    name: string
    url: string
    feature: string
    ingredients: string
    options: Object[]
    images: Object[]
}
