import {
    Body,
    Controller, Delete,
    Get,
    Param,
    Post, Put, Query,
} from '@nestjs/common';

import { Product } from "./entity/product.entity";
import { ProductsService } from "./products.service";
import { CreateProductDto } from "./dto/create-product.dto";
import { ImagesService } from "../images/images.service"
import { response } from "express";

@Controller('products')
export class ProductsController {
    constructor(
        private readonly productsService: ProductsService) {
    }

    @Get()
    findAll(@Query('list') list): Promise<Product[]> {
        return this.productsService.findAll(list);
    }

    @Get('category/:id')
    findByCategoryId(@Param('id') id: number): Promise<Product[]> {
        return this.productsService.findByCategoryId(id);
    }

    @Post()
    create(@Body() CreateProductDto: CreateProductDto): Promise<Product> {
        return this.productsService.create(CreateProductDto);
    }

    @Put('update/:id')
    update(@Param('id') id: number, @Body() CreateOptionDto: CreateProductDto) {
        return this.productsService.update(id, CreateOptionDto);
    }

    @Delete('delete/:id')
    delete(@Param('id') id: number) {
        return this.productsService.delete(id);
    }

}
