import { Body, Injectable, Post } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { Product } from "./entity/product.entity";
import { CreateProductDto } from "./dto/create-product.dto";

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(Product) private readonly productsRepository: Repository<Product>,
    ) {
    }

    async findAll(list): Promise<Product[]> {
        return this.productsRepository
            .createQueryBuilder('products')
            .leftJoinAndSelect('products.images', 'images')
            .leftJoinAndSelect('products.options', 'options', (list) ? '' : 'options.isActive != false')
            .leftJoinAndSelect('products.category', 'category')
            .leftJoinAndSelect('options.data', 'data')
            .orderBy('products.id', 'ASC')
            .addOrderBy('images.id', 'ASC')
            .addOrderBy('options.order', 'ASC')
            .addOrderBy('data.type', 'ASC')
            .where((list) ? '' : 'products.isActive != false')
            .getMany();
    }

    async findByCategoryId(id): Promise<Product[]> {
        return this.productsRepository
            .createQueryBuilder('products')
            .where("products.categoryId = :id", {id})
            .leftJoinAndSelect('products.images', 'images')
            .leftJoinAndSelect('products.options', 'options')
            .leftJoinAndSelect('products.category', 'category')
            .leftJoinAndSelect('options.data', 'data')
            .orderBy('products.id', 'ASC')
            .addOrderBy('images.id', 'ASC')
            .addOrderBy('data.type', 'ASC')
            .addOrderBy('options.order', 'ASC')
            .where('products.isActive != false')
            .getMany();
    }

    async findByIds(ids): Promise<Product[]> {
        return this.productsRepository.findByIds(ids);
    }

    async create(CreateProductDto: CreateProductDto): Promise<Product> {
        return await this.productsRepository.save(CreateProductDto);
    }

    async update(id: number, CreateProductDto: CreateProductDto) {
        console.log(CreateProductDto)
        return this.productsRepository.save(CreateProductDto);
    }

    async delete(id: number) {
        return this.productsRepository.delete(id);
    }

}
