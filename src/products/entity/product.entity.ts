import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    JoinTable,
    ManyToMany,
    OneToMany,
    JoinColumn,
    OneToOne, ManyToOne
} from 'typeorm';
import { ProductOption } from "./product-option.entity";
import { Image } from "../../images/image.entity";
import { Category } from "../../categories/category.entity";

@Entity('products')
export class Product {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    categoryId: number;

    @ManyToOne(() => Category)
    @JoinColumn()
    category: Category;

    @Column()
    name: string;

    @Column()
    url: string;

    @Column({nullable: true})
    feature: string;

    @Column({nullable: true})
    ingredients: string;

    @Column({default: false})
    isActive: boolean;

    @Column({default: 1})
    order: number;

    @OneToMany(type => ProductOption, option => option.product, {
        cascade: true,
    })
    options: ProductOption[];

    @OneToMany(type => Image, image => image.product, {
        cascade: true,
    })
    @JoinColumn()
    images: Image[];

    @Column('simple-array', {nullable: true})
    similar: number[];

    @Column('json', { nullable: true })
    meta: string;
}
