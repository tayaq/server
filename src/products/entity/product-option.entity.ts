import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, BaseEntity, JoinTable } from 'typeorm';
import { Product } from "./product.entity";
import { Category } from "../../categories/category.entity";
import { Option } from "../../options/option.entity";

@Entity('products_options', {
    orderBy: {
        price: "ASC"
    }
})
export class ProductOption {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true})
    crmId: string;

    @Column()
    optionId: number;

    @ManyToOne(type => Option, {
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    })
    @JoinColumn({name: 'optionId'})
    data: Option;

    @Column({default: 0})
    price: number;

    @Column({default: 0})
    weight: number;

    @Column()
    productId: number;

    @Column({default: false})
    isActive: boolean;

    @Column({default: 1})
    order: number;

    @ManyToOne(() => Product, product => product.options, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    })
    product: Product;
}
