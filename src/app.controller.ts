import {Controller, Get, HttpException, HttpStatus} from '@nestjs/common';
import {AppService} from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {

    }

    // @Get()
    // async anyText() {
    //     var reader = require('any-text');
    //
    //     const text = await reader.getText('test.pdf');
    //     console.log(text.trim().trimEnd().length);
    //     return 123;
    // }

}
