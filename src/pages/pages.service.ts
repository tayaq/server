import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

import { Page } from "./page.entity";
import { Option } from "../options/option.entity";
import { CreatePageDto } from "./dto/create-page.dto";

@Injectable()
export class PagesService {
    constructor(
        @InjectRepository(Page)
        private readonly pageRepository: Repository<Page>
    ) {
    }

    findAll(): Promise<Page[]> {
        return this.pageRepository.find();
    }

    async findOne(page): Promise<Page> {
        let result = await this.pageRepository.findOne({url: page});
        if (!result) throw new NotFoundException();
        return result;
    }

    async create(CreatePageDto: CreatePageDto): Promise<Page> {
        return this.pageRepository.save(CreatePageDto);
    }

    async update(id: number, CreatePageDto: CreatePageDto) {
        return this.pageRepository.update(id, CreatePageDto);
    }

    async delete(id: number) {
        return this.pageRepository.delete(id);
    }

}
