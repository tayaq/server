import { IsEmail, IsEmpty, IsNotEmpty } from 'class-validator';

export class CreatePageDto {
    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    url: string

    @IsNotEmpty()
    content: string

    @IsNotEmpty()
    title: string

    @IsNotEmpty()
    description: string

    @IsNotEmpty()
    keywords: string
}
