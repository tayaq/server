import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put } from '@nestjs/common';

import { Page } from "./page.entity";
import { PagesService } from "./pages.service";
import { CreatePageDto } from "./dto/create-page.dto";

@Controller('pages')
export class PagesController {
    constructor(private readonly pagesService: PagesService) {
    }

    @Get()
    findAll(): Promise<Page[]> {
        return this.pagesService.findAll();
    }

    @Get(':page')
    async findOne(@Param('page') page: string): Promise<Page> {
        let result = await this.pagesService.findOne(page)
        console.log(result);
        // if (result.id) throw new HttpException('Page not found', HttpStatus.NOT_FOUND);
        return result;
    }
    @Post('create')
    createOption(@Body() CreatePageDto: CreatePageDto): Promise<Page> {
        return this.pagesService.create(CreatePageDto);
    }

    @Put('update/:id')
    updateOption(@Param('id') id: number, @Body() CreatePageDto: CreatePageDto) {
        return this.pagesService.update(id, CreatePageDto);
    }

    @Delete('delete/:id')
    deleteOption(@Param('id') id: number) {
        return this.pagesService.delete(id);
    }

}
