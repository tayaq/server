import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { HtmlTags } from "../shared/html-tags";

@Entity('pages')
export class Page extends HtmlTags {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    url: string;

    @Column()
    content: string;
}
