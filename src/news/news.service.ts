import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { News } from "./news.entity";
import { Page } from "../pages/page.entity";

@Injectable()
export class NewsService {
    constructor(
        @InjectRepository(News)
        private readonly newsRepository: Repository<News>
    ) {
    }

    findAll(): Promise<News[]> {
        return this.newsRepository.find();
    }

    async findOne(id): Promise<News> {
        let result = await this.newsRepository.findOne({id});
        if (!result) throw new NotFoundException();
        return result;
    }

}
