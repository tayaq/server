import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { News } from "./news.entity";
import { NewsService } from "./news.service";
import { Page } from "../pages/page.entity";

@Controller('news')
export class NewsController {

    constructor(private readonly newsService: NewsService) {
    }

    @Get()
    findAll(): Promise<News[]> {
        return this.newsService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: number): Promise<News> {
        return this.newsService.findOne(id)
    }

    // @Post('create')
    // createOption(@Body() CreatePageDto: CreatePageDto): Promise<Page> {
    //     return this.pagesService.create(CreatePageDto);
    // }
    //
    // @Put('update/:id')
    // updateOption(@Param('id') id: number, @Body() CreatePageDto: CreatePageDto) {
    //     return this.pagesService.update(id, CreatePageDto);
    // }
    //
    // @Delete('delete/:id')
    // deleteOption(@Param('id') id: number) {
    //     return this.pagesService.delete(id);
    // }

}
