import { PrimaryGeneratedColumn, CreateDateColumn, Entity, Column } from "typeorm";

@Entity('news')
export class News {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    desc: string;

    @Column()
    image: string;

    @Column()
    url: string;

    @CreateDateColumn()
    created: string;

}
