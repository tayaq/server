import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import * as csurf from 'csurf';
import * as helmet from 'helmet';
import {ValidationPipe} from "@nestjs/common";
require('dotenv').config()


async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.enableCors();
    // app.use(helmet());
    // app.use(csurf());
    app.useGlobalPipes(new ValidationPipe({transform: true}));
    await app.listen(3003);
}

bootstrap();
