import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { Option } from "./option.entity";
import { CreateOptionDto } from "./dto/create-option.dto";

@Injectable()
export class OptionsService {

    constructor(
        @InjectRepository(Option) private readonly optionRepository: Repository<Option>,
    ) {
    }

    async findAll(): Promise<Option[]> {
        return this.optionRepository.find();
    }

    async findByCategory(id): Promise<Option[]> {
        return this.optionRepository.find({
            where: { categoryId: id },
            order: {
                order: "ASC"
            }
        });
    }

    async createOption(CreateOptionDto: CreateOptionDto): Promise<Option> {
        return this.optionRepository.save(CreateOptionDto);
    }

    async updateOption(id: number, CreateOptionDto: CreateOptionDto) {
        return this.optionRepository.update(id, CreateOptionDto);
    }

    async deleteOption(id: number) {
        return this.optionRepository.delete(id);
    }
}
