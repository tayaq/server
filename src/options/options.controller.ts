import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

import { Option } from "./option.entity";
import { OptionsService } from "./options.service";
import { CreateOptionDto } from "./dto/create-option.dto";

@Controller('options')
export class OptionsController {

    constructor(private readonly optionsService: OptionsService) {
    }

    @Get()
    findAll(): Promise<Option[]> {
        return this.optionsService.findAll();
    }

    @Get('/category/:url')
    findByCategory(@Param('url') url: string): Promise<Option[]> {
        return this.optionsService.findByCategory(url);
    }

    @Post('create')
    createOption(@Body() CreateOptionDto: CreateOptionDto): Promise<Option> {
        return this.optionsService.createOption(CreateOptionDto);
    }

    @Put('update/:id')
    updateOption(@Param('id') id: number, @Body() CreateOptionDto: CreateOptionDto) {
        return this.optionsService.updateOption(id, CreateOptionDto);
    }

    @Delete('delete/:id')
    deleteOption(@Param('id') id: number) {
        return this.optionsService.deleteOption(id);
    }

}
