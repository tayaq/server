import {IsEmail, IsEmpty, IsNotEmpty} from 'class-validator';

export class CreateOptionDto {
    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    categoryId: number
}
