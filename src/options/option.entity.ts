import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Product } from "../products/entity/product.entity";

enum optionType {
    SIZE = 1,
    TOPPING = 2,
    COMBO = 3
}

@Entity('options')
export class Option {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    categoryId: number;

    @Column()
    name: string;

    @Column({default: 1})
    order: number

    @Column("enum", {enum: optionType, default: 1})
    type: optionType;
}
